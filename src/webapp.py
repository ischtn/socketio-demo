import json

import jinja2
import socketio
from aiohttp import web
import aiohttp_jinja2

sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)


async def index(request):
	"""Serve the client-side application."""

	items = request.app.get('items', None)
	context = {}
	context['keys'] = items

	return aiohttp_jinja2.render_template('index.html', request, context)


@sio.on('connect')
def connect(sid, environ):
	print(f"msg: socket {sid} connected")


@sio.on('message')
async def message(data):
	print(f"msg: {data}")

	payload = json.dumps(data)
	data['data'] = payload
	await sio.emit('message', data=data)


@sio.on('button_event')
async def button_event(event, data):
	print(f"msg: {data}")

	payload = json.dumps(data)
	data['data'] = payload
	await sio.emit('message', data=data)


@sio.on('disconnect')
def disconnect(sid):
	print(f"msg: socket {sid} disconnected")


template_path = 'templates/'
static_path = 'static'

app.router.add_static('/static/', path=static_path, name='static')
app.router.add_get('/', index)
app['static_root_url'] = static_path

aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(template_path))
