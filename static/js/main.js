
$(function () {
	$('[data-toggle="tooltip"]').tooltip()
});


var socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on('connect', function() {
	socket.emit('my event', {data: 'I\'m connected!'});
});

socket.on('message', function(message) {
	let payload = message['data'];
	const box = document.getElementById('console');

	box.innerHTML += message['data'] + "<br />";

	payload = JSON.parse(payload);

	let key = Object.keys(payload)[0];
	let value = Object.values(payload)[0];
	const item = document.getElementById(key);
	item.innerText = value;

});

socket.on('reply', function(message) {
	let payload = message['data'];

	const box = document.getElementById('test');
	box.innerHTML += payload + "<br />";
});

function sendMessage(){
	socket.emit('button_event', {data: 'I pressed the button!'});
}